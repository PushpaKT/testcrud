<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');

Route::post('/getdata', 'AjaxController@getdata');

Route::post('/removedata', 'AjaxController@removedata');
Route::get('/detail', 'AjaxController@detail');
Route::get('/sampleDrop', 'MainController@sampleDrop');
Route::post('/getdata1', 'MainController@getdata1');
//Route::post('/getcity','MainController@getcity');
Route::get('/getcity/{id}',function($id){
	return App::make('App\Http\Controllers\AjaxController')->getcity($id);
});
Route::get('/getedituser/{id}',function($id){
	return App::make('App\Http\Controllers\AjaxController')->getedituser($id);
});