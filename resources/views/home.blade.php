<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
		<style>
			* {
			    box-sizing: border-box;
			}

			input[type=text], input[type=email], input[type=number],select {
			    width: 50%;
			    padding: 12px;
			    border: 1px solid #ccc;
			    border-radius: 4px;
			    resize: vertical;
			}

			label {
			    padding: 12px 12px 12px 0;
			    display: inline-block;
			}
			a{
				 background-color: #ADD8E6;
				  color: black;
			    padding: 6px 10px;
			    border: none;
			    border-radius: 4px;
			    cursor: pointer;
			    float: center;
			}

			input[type=submit], input[type=button] {
			    background-color: #4CAF50;
			    color: white;
			    padding: 12px 20px;
			    border: none;
			    border-radius: 4px;
			    cursor: pointer;
			    float: right;
			}

			input[type=submit]:hover {
			    background-color: #45a049;
			}

			input[type=button]:hover {
			    background-color: #45a049;
			}

			.container {
			    border-radius: 5px;
			    background-color: #f2f2f2;
			    padding: 20px;
			}

			.col-25 {
			    float: left;
			    width: 25%;
			    margin-top: 6px;
			}

			.col-75 {
			    float: left;
			    width: 75%;
			    margin-top: 6px;
			}

			/* Clear floats after the columns */
			.row:after {
			    content: "";
			    display: table;
			    clear: both;
			}

			/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
			@media screen and (max-width: 600px) {
			    .col-25, .col-75, input[type=submit] {
			        width: 100%;
			        margin-top: 0;
			    }
			}

			table {
				width:100%;
			}
			table, th, td {
				border: 1px solid black;
				border-collapse: collapse;
			}
			th, td {
				padding: 15px;
				text-align: left;
			}
		</style>

		<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      
      <script>
      	$(document).ready(function($){
		 $('#state').change(function (ev){
		var state = $(this).val();
		var _token = $('input[name=_token]').val();
		citydrop(state , _token);
	});
});
      	function citydrop(state,_token)
      	{
      		$("#city").html('');
		var select ='';
		$.get('/getcity/'+state,function(data){
			if(data)
			{
			  $.each(data, function(k,v) {
			 	 $("#city").append($('<option></option>').val(v.id).html(v.name));
			   });
			 }
		});
      	}
         function sform(){
            
            var formdata = $('#Myform').serialize();
            var flag=$('#num').val();
            if(validate()){
             $.post("{{url('getdata')}}",formdata,function(res){
            	if (res == '1') {
            		alert('Success! Data Saved');
            		if(flag == '0' || flag == '')
            		{
            			//alert(flag);
            			$('#city').val('');
            			document.getElementById("Myform").reset();
            		}
            		$("#mydiv").load(location.href + " #mydiv");
            	} else {
            		alert('Error! Data could not be added');
            	}
            });
         }

       }
     function validate()
     {
     	var name =$('#name').val();
     	var email =$('#email').val();
     	var mobile =$('#mobile').val();
     	var state =$('#state').val();
     	var city =$('#city').val();
     	var female =$('#gender');
     	var gen =$('input[name=male]:checked').val();
     	$(".err").html("");
     	//alert(gen);
     	if(name=='')
		{
		 $("<span class='text-danger err'>Please enter name</span>" ).insertAfter('#name');
		 return false;		  
		}
		else if(email == ''){
			 $("<span class='text-danger err'>Please enter email</span>" ).insertAfter('#email');
		 return false;	
		}
		else if(mobile == ''){
			 $("<span class='text-danger err'>Please enter mobile</span>" ).insertAfter('#mobile');
		 return false;	
		}
		else if(state == ''){
			 $("<span class='text-danger err'>Please Select state</span>" ).insertAfter('#state');
		 return false;	
		}
		else if(city == ''){
			 $("<span class='text-danger err'>Please Select city</span>" ).insertAfter('#city');
		 return false;	
		}
		else if(! gen){
			 $("<span class='text-danger err'>Please Select gender.</span>" ).insertAfter('#gender');
		 return false;	
		}
		else{
			return true;
		}
     }

         function edit(n)
         {
         	/*var name = $('#uname'+n).html(),
         		email = $('#uemail'+n).html(),
         		mobile = $('#umobi'+n).html();
         		state = $('#ustate'+n).html();
         		city = $('#ucity'+n).html();
         		//alert(city);
                setTimeout(function() {
         		citydrop(state);

         	}, 1000);
         	$('#name').val(name);
         	$('#email').val(email);
         	$('#mobile').val(mobile);
         	$('#num').val(n);
         	$('#state').val(state);
            
         	$('#city').val(city);*/

         	  	$.get('/getedituser/'+n,function(data){
         		if(data)
         		{
         			$("#name").val(data["name"]);
         			$("#email").val(data["email"]);
         			$("#mobile").val(data["contact"]);
         			$("#state").val(data["state"]);
                     let b=data["status"];
                     if(data["gender"] === 'female')
                     {
                     	$('#female').prop('checked', true);
                     }
                     else
                     {
                     	$('#male').prop('checked', true);
                     }
                     if(b === 0)
         			{
         				$('#check').prop('checked', false); 
         				
         			 }
         			else{
         					$('#check').prop('checked', true);
         				}
         			//$("#check").val(data["status"]);
         			setTimeout(function() {
         		citydrop(data["state"]);

         	}, 1000);
         			$("#city").val(data["city"]);


         		}
         		$("#num").val(n);

         	});
         }

         function dele(n)
         {
         	var r = confirm("Are you sure to delete this data!");

			if (r == true) {

				$('#delnum').val(n);

				var formdata = $('#Myform').serialize();

			    $.post("{{url('removedata')}}",formdata,function(res){

	            	if (res == '1') {

	            		alert('Success! Data Removed');
	            		$("#mydiv").load(location.href + " #mydiv");

	            	} else {

	            		alert('Error! Data could not be removed');
	            	}

	            });
			} else {
			    return false;
			}
         }

      </script>
    </head>
    <body>
        <h2>Information Form</h2>
		<p></p>

		<div class="container">
			<form id="Myform">
				{{ csrf_field() }}
				<input type="hidden" name="num" id="num" value="0">
				<input type="hidden" name="delnum" id="delnum" value="0">
				<div class="row">
					<div class="col-25">
						<label for="name">Name</label>
					</div>
					<div class="col-75">
						<input type="text" id="name" name="name" placeholder="Your name..">
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="email">Email</label>
					</div>
					<div class="col-75">
						<input type="email" id="email" name="email" placeholder="Your Email Address..">
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="mobile">Mobile Number</label>
					</div>
					<div class="col-75">
						<input type="number" id="mobile" name="mobile" placeholder="Your Mobile Number..">
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="state">State</label>
					</div>
					<div class="col-75">
						<select id="state" name="state">
						<option value="">Select State</option>
						@foreach($states as $state)
						<option value="{{$state->id}}">{{$state->name}}</option>
						@endforeach
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="city">city</label>
					</div>
					<div class="col-75">
						<select id="city" name="city">
						<option value="">select city</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="email">Gender</label>
					</div>
					<div id="gender" class="col-75">
						<input type="radio" id="male" name="male" value="male"> Male
						<input type="radio" id="female" name="male" value="female"> Female
					</div>
				</div>
				<div class="row">
					<div class="col-25">
					<input type="checkbox" name="check" id="check">
					</div>
					<div class="col-75">
						<input type="button" onclick="sform()" value="Submit">
					</div>
					
				</div>
			</form>
		</div>
		<div id="mydiv">
			@if(count($users) > 0)
			<hr>
			<div class="container">
				<table>
					<tr>
						<th>Name</th>
						<th>Email</th> 
						<th>Mobile</th>
						<th>State</th>
						<th>City</th>
						<th>Action</th>
						
					</tr>
					@foreach($users as $user)
					<tr>
						<td><span id="uname{{$user->id}}">{{$user->name}}</span></td>
						<td><span id="uemail{{$user->id}}">{{$user->email}}</span></td>
						<td><span id="umobi{{$user->id}}">{{$user->contact}}</span></td>
						<td><span id="ustate{{$user->id}}">{{$user->s_name}}</span></td>
						<td><span id="ucity{{$user->id}}">{{$user->c_name}}</span></td>
						<td>
							<a class="btn"href="javascript:;" onclick="edit({{$user->id}})">Edit</a>
							<a href="javascript:;" onclick="dele({{$user->id}})">Delete</a>
						</td>
					</tr>
					@endforeach()
				</table>
			</div>
			@endif
		</div>
    </body>
</html>
