<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Response;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function index(){

       $users =DB::table('users as users')
                ->join('state as state', 'users.state', '=', 'state.id')
                ->join('city as city', 'users.city', '=', 'city.id')
               ->select('users.id','users.name as name','users.email','users.contact','city.city_name as c_name','state.name as s_name')
                ->get();//
               // dd($users);
          // $users= \App\user::all();
           $states = \App\state::all();
       
    	return view('home',compact('users','states'));
    }
    public function sampleDrop()
    {
    	$users = \App\user::all();
    	$states = DB::table('state')->get();//\App\state::all();
    	dd($states);
    	return view('sampleDrop',compact('users','states'));
    }
    public function getdata1(Request $request)
    {

    	$i = new \App\password_resets;

	    	$i->email=$request->SampleID ? $request->SampleID : '';
	    	$i->token=$request->get('_token')? $request->get('_token') :'';
	    	$i->user_id=$request->user_id ?$request->user_id : '';
	    	//dd($i);
	    		if ($i->save()) {

	        	$return = '1';

	        } else {

	        	$return = '0';
	        }
    }
    public function getcity(Request $request)
    {
       //return $request;
    	$state= $request->state;
    	//dd($state);
    	$city=DB::table('city')->select('city_name as c_name','id as c_id')->where('state_id',$state)->get();
    	//return \Responce::json($city);
    	//dd($city);
    	return Response::JSON($city);
    }
}
