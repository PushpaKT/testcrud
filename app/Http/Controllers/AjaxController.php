<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Response;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function getdata(Request $request){
    	//return $request;
    	$id = $request->get('num');

    	$return = "";

    	if ($id == "0" || $id == "") {
    		$status=0;
    		if( !is_null($request->get('check')))
    		{
    			$status=1;
    		}
    		$i = new \App\user;

	    	$i->name=$request->get('name');
	    	$i->email=$request->get('email');
	    	$i->contact=$request->get('mobile');
	    	$i->password=$request->get('name').'_'.$request->get('mobile');
	    	$i->state=$request->get('state');
	    	$i->city=$request->get('city');
	    	$i->status=$status;
	    	$i->gender=$request->get('male')? $request->get('male') : $request->get('female');
	    	//dd($i);

	    	if ($i->save()) {

	        	$return = '1';

	        } else {

	        	$return = '0';
	        }

    	} else {
    		//return $request;
    		$status=0;
    		if( !is_null($request->get('check')))
    		{
    			$status=1;
    		}
    		DB::beginTransaction();
    		$u = \App\user::find($id);

    		$u->name=$request->get('name');
	    	$u->email=$request->get('email');
	    	$u->contact=$request->get('mobile');
	    	$u->password=$request->get('name').'_'.$request->get('mobile');
	    	$u->state=$request->get('state');
	    	$u->city=$request->get('city');
	    	$u->status=$status;
	    	$u->gender=$request->get('male')? $request->get('male') : $request->get('female');

	    	if ($u->save()) {
	    		DB::commit();

	        	$return = '1';

	        } else {

	        	$return = '0';
	        }
    	}

    	return $return;
    }

    public function removedata(Request $request){

    	$id = $request->get('delnum');

    	$return = "";

    	$d = \App\user::find($id);

        if($d->delete())
        {
        	$return = '1';
        }
        else
        {
        	$return = '0';
        }

        return $return;
    }
    public function detail()
    {
    	$data['documet']='adsad.doc';
    	$data['claim']='sdsda';
    	$data['customername']='Mr.XYZ';
    	//dd($data);	
    	return view('detail',$data);
    }
    public function getcity($id)
    {
       //return $request;
    	//$state= $request->state;
    	//dd($state);
    	$city=DB::table('city')->select('city_name as name','id as id')->where('state_id',$id)->get();
    	//return \Responce::json($city);
    	//dd($city);
    	return Response::JSON($city);
    }
    public function getedituser($id)
    {
    	$useredit=\App\user::find($id);
    	return Response::JSON($useredit);
    }
    
}
